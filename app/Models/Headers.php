<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Headers extends Model
{
    protected $table = 'service_headers';
}
