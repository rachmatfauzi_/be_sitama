<?php

namespace App\Http\Controllers;

use App\Models\Details;
use App\Models\Headers;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = [];
            $headers = Headers::all('id', 'service_name');
            $details = Details::all('id', 'service_id','group', 'description','mandatory','sequence','parent','type','is_multiple','remark');

            foreach($headers as $header) {
                $services_json['json'] = [];
                foreach ($details as $element) {
                    if($header['id'] == $element['service_id']) {
                        $services_json['service_id'] = $header;
                        $services_json['json'][] = [
                            "id"=> $element['id'],
                            "group"=> $element['group'],
                            "parent_description"=> $element['parent_description'],
                            "description"=> $element['description'],
                            "mandatory"=> $element['mandatory'],
                            "sequence"=> $element['sequence'],
                            "parent"=> $element['parent'],
                            "type"=> $element['type'],
                            "is_multiple" => $element['is_multiple'],
                            "remark"=> $element['remark'],
                            "value"=> $element['value']
                        ];
                    }
                }
                $data[] = $services_json;
            }
            return response()->json([
                "code" => 200,
                "status" => "Success",
                "data" => [
                    "orderid" => "",
                    "orderno" => "",
                    "services" => $headers,
                    "services_json" => $data
                ],
                "time_stamp" => date("Y-m-d H:i:s")
            ]);
        } catch (Exception $e){
            return response()->json([
                "code" => 500,
                "status" => "Failed",
                "message" => $e,
                "time_stamp" => date("Y-m-d H:i:s")
            ]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(['message'=>'halo '.$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
